class Message:
	def __init__(self, line):

		self.mid = 0x0000
		self.line = line
		# Break into pieces
		pieces = line.split(',')

		if len(pieces) > 3:
			self.idh = pieces[0].split(':')[1].strip()
			self.idl = pieces[1].split(':')[1].strip()
			self.mid = self.idh + self.idl
			self.data = pieces[2].split(':')[1].strip()

			# Get Time Stamp
			self.TS = pieces[3].split(':')[1]


# Parser for Raw data
class Converter:
	def __init__(self, line):

		self.mid = 0x0000
		self.line = line
		pieces = line.split(',')

		if len(pieces) > 3:
			self.time = pieces[1]
			self.rel_time = pieces[2]
			self.status = pieces[3]
			self.error = pieces[4]
			self.tx = pieces[5]
			self.description = pieces[6]
			self.network = pieces[7]
			self.node = pieces[8]
			self.mid = pieces[9]
			self.target = pieces[10]
			self.source = pieces[11]
			self.b1 = pieces[12]
			self.b2 = pieces[13]
			self.b3 = pieces[14]
			self.b4 = pieces[15]
			self.b5 = pieces[16]
			self.b6 = pieces[17]
			self.b7 = pieces[18]
			self.b8 = pieces[19]
			# Time Stamp variable
			self.TS = pieces[1]
			# Combined Data
			self.data = self.b1 + self.b2 + self.b3 + self.b4 + self.b5 + self.b6 + self.b7 + self.b8
			# Handle Tx/Rx errors
			if len(self.mid) > 4:
				self.mid = 'Rx_Tx_Error'
			else:
				pass


# Parser for M & V data
class Message_MV:
	def __init__(self, line):

		self.mid = 0x0000
		self.line = line
		# Break into pieces
		pieces = line.split(',')

		if len(pieces) > 3:
			self.idh = pieces[0].split(':')[1].strip()
			self.idl = pieces[1].split(':')[1].strip()
			self.mid = self.idh + self.idl
			self.len = pieces[2].split(':')[1].strip()
			self.data = pieces[3].split(':')[1].strip()

			# Get Time Stamp
			self.TS = pieces[3].split(':')[2]

			# addition of timestamp require to remove it
			data_ts = pieces[3].split(':')[1]
			ts_offset = data_ts.find("TS")

			if ts_offset != 0:
				self.data = data_ts[:ts_offset].strip()
			else:
				self.data = data_ts.strip()
