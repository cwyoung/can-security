#! /usr/bin/python
from Message import Message, Converter, Message_MV
import sys
import csv


def convert(input_file, output_file):
	out = open(output_file, "w")
	raw_message = "Time, Relative time, status, error, tx, description, network, node, mid, target, source, data bits"
	f = open(input_file, "r")
	output = open('ALL_CAN_out.csv', 'wb')
	for line in f:
		#print "line"
		#print line
		msg = Converter(line)
		#print "message id"
		#print msg.mid
		id = str(msg.mid)
		try:
			idh = "0" + "%s" % (msg.mid[0])
			idl = msg.mid[1] + msg.mid[2]
		except IndexError:
			idh = "00"
			idl = msg.mid[0] + msg.mid[1]
		time = msg.time
		data = msg.b1 + msg.b2 + msg.b3 + msg.b4 + msg.b5 + msg.b6 + msg.b7 + msg.b8
		message = "IDH: %s, IDL: %s, Data: %s, TS: %s" % (idh, idl, data, time)
		#print message
		out.write(message + '\n')
		# Output all CAN data into CSV
		mid = msg.mid
		all_data = [time, mid, data]
		writer = csv.writer(output)
		writer.writerow(all_data)
	output.close()

def convert_MV(input_file, output_file):
	out = open(output_file, "w")
	f = open(input_file, "r")
	for line in f:
		#print "line"
		#print line
		msg = Message_MV(line)
		#print "message id"
		#print msg.mid
		id = str(msg.mid)
		idh = msg.idh
		idl = msg.idl
		time = msg.TS
		data = msg.data
		message = "IDH: %s, IDL: %s, Data: %s, TS: %s" % (idh, idl, data, time)
		#print message
		out.write(message)


# Read log and get metrics [#Msgs, Msg IDs]
def reading_log(input_file):

	mids = []
	messages = {}
	total_messages = 0
	f = open(input_file, "r")
	for line in f:
		msg = Message(line)
		if msg.mid not in mids:
			total_messages += 1
			mids.append(msg.mid)
			u = {msg.mid: {"Count": 1}}
			messages.update(u)
		elif msg.mid in mids:
			total_messages += 1
			count = int(messages[msg.mid]["Count"]) + 1
			u = {msg.mid: {"Count": count}}
			messages.update(u)
	f.close()
	print "Total Messages: %i" % total_messages
	print "Number of CAN IDs: %i" % len(messages)
	print messages
	return messages


# Creating new log file based on specific metrics
# Attack log: 2x,5x,10x Msg ID injection
def writing_log(input_file, output_file):
	time = 0
	out = open(output_file, "w")
	packet = "IDH: %02X, IDL: %02X, Len: 08, Data: "
	# out.write(packet + '\n')
	f = open(input_file, "r")
	for line in f:
		time += .001
		msg = Message(line)
		msg.TS = time
		message = "IDH: %02X, IDL: %02X, Data: %s, TS: %s" % (int(msg.idh, 16), int(msg.idl, 16), msg.data, msg.TS)
		#print message
		out.write(message + '\n')

	f.close()


if __name__ == "__main__":

	input_file = "all_driving.dat"
	#output_file = "input_data_MV.dat"
	output_file = "input_data.dat"
	#input_file = "fusion_attack_raw.dat"
	#output_file = "fusion_attack.dat"
	# Convert Raw Data
	#convert(input_file, output_file)
	# Convert M&V Data
	#convert_MV(input_file, output_file)

	#writing_log(input_file, output_file)
	reading_log(output_file)
