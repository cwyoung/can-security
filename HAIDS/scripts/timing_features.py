from Message import Message, Converter
import detectors


# Gets normal values for Message timings
def get_timing(input_file, mids):

	message_timings = {}
	time_old = {}
	f = open(input_file, "r")
	for line in f:
		# Raw Data
		msg = Converter(line)
		# M&V Data
		# msg = Message(line)
		if msg.mid not in mids:
			mids.append(msg.mid)
			u = {msg.mid: {"Time Stamp": msg.TS, "Message Frequency": 0, "Message Interval": 0}}
			message_timings.update(u)
			t = {msg.mid: {"Time Stamp": msg.TS, "Message Interval": 0}}
			time_old.update(t)
		elif msg.mid in mids:
			# Calculate Message interval and msg_freq = interval average
			msg_interval = float(msg.TS)-float(time_old[msg.mid]["Time Stamp"])
			msg_freq = float((msg_interval+time_old[msg.mid]["Message Interval"])/2)
			time_old.update({msg.mid: {"Time Stamp": msg.TS, "Message Interval": msg_interval}})
			u = {msg.mid: {"Time Stamp": msg.TS, "Message Frequency": msg_freq, "Message Interval": msg_interval}}
			message_timings.update(u)
	f.close()
	print "Message Timings", message_timings
	return message_timings


# Gets normal values for Message frequencies
def get_frequency(input_file, mids):

	message_frequencies = {}
	old_frequency = {}
	time = 0
	time_interval = 2
	message_log = {}
	f = open(input_file, "r")
	for line in f:
		# Raw Data
		msg = Converter(line)
		# M&V Data
		# msg = Message(line)
		if time == 0:
			time = msg.TS
		else:
			pass
		if msg.mid not in mids:
			mids.append(msg.mid)
			u = {msg.mid: {"Time Stamp": msg.TS, "Message Frequency": 0, "Message Count": 1}}
			message_frequencies.update(u)
			t = {msg.mid: {"Time Stamp": msg.TS, "Message Frequency": 0, "Message Count": 0}}
			old_frequency.update(t)
			m = {msg.mid: {"Time": [], "Frequency": []}}
			message_log.update(m)
		elif msg.mid in mids:
			# Calculate message frequency
			if float(msg.TS) - float(time) < float(time_interval):
				message_frequencies[msg.mid]["Message Count"] += 1
				message_frequencies[msg.mid]["Time Stamp"] = msg.TS
			else:
				for i in mids:
					frequency = message_frequencies[i]["Message Count"]
					if old_frequency[i]["Message Frequency"] == 0:
						old_frequency[i]["Message Frequency"] = frequency
						message_frequencies[i]["Message Frequency"] = frequency
						message_frequencies[i]["Message Count"] = 0
					else:
						average_frequency  = (frequency+(old_frequency[i]["Message Frequency"]))/2
						old_frequency[i]["Message Frequency"] = frequency
						message_frequencies[i]["Message Frequency"] = average_frequency
						message_frequencies[i]["Message Count"] = 0
						message_log[i]["Time"].append(msg.TS)
						message_log[i]["Frequency"].append(average_frequency)
				time = float(msg.TS)
	f.close()
	print "Message Frequencies", message_frequencies
	return message_frequencies


# Checks Timing of the Messages and indicates if timing is out of normal range.
def interval_checker(input_file, mids, message_timings):

	# {"Msg_ID": msg.mid, "Time Stamp": msg.ts, "Frequency Range": x}
	i = 0
	anomalies = 0
	past_anomalies = 0
	msg_id = []
	message = {}
	time_old = {}
	new_message = {}
	msg_timing = message_timings
	# Message Frequency Range constant for each MID
	# {msg.mid: message_freq}
	f = open(input_file, "r")
	for line in f:
		i = i + 1
		# Get Message ID
		# Raw Data
		msg = Converter(line)
		# M&V Data
		# msg = Message(line)
		# Need to set up old_time for every MSG ID before frequency check
		if msg.mid not in msg_id:
			msg_id.append(msg.mid)
			u = {msg.mid: {"Time Stamp": msg.TS, "Message Frequency": 0, "Message Interval": 0, "Anomaly Counter": 0}}
			message.update(u)
			t = {msg.mid: {"Time Stamp": msg.TS, "Message Interval": 0}}
			time_old.update(t)
		# Get Message Time
		elif msg.mid in msg_id:
			# Calculate Message Frequency
			msg_interval = float(msg.TS) - float(time_old[msg.mid]["Time Stamp"])
			msg_freq = float((msg_interval + time_old[msg.mid]["Message Interval"]) / 2)
			t = {msg.mid: {"Time Stamp": msg.TS, "Message Interval": msg_interval}}
			time_old.update(t)
			u = {msg.mid: {"Time Stamp": msg.TS, "Message Frequency": msg_freq, "Message Interval": msg_interval, "Anomaly Counter": message[msg.mid]["Anomaly Counter"]}}
			message.update(u)
			try:
				if (float(msg_timing[msg.mid]["Message Frequency"]) - (float(msg_timing[msg.mid]["Message Frequency"]) * 0.5)) \
					<= float(message[msg.mid]["Message Frequency"]) <= \
					(float(msg_timing[msg.mid]["Message Frequency"]) + (float(msg_timing[msg.mid]["Message Frequency"]) * 0.5)):
					# Check that frequency is within range
					# L = (float(msg_timing[msg.mid]["Message Frequency"]) - (float(msg_timing[msg.mid]["Message Frequency"]) * 0.25))
					# H = (float(msg_timing[msg.mid]["Message Frequency"]) + (float(msg_timing[msg.mid]["Message Frequency"]) * 0.25))
					# print "Message: %s, Current: %s, Range: %s, %s" % (msg.mid, message[msg.mid]["Message Frequency"], L,H)
					pass
				else:
					if float(msg_timing[msg.mid]["Message Frequency"]) == 0.0:
						msg_timing[msg.mid]["Message Frequency"] = msg_freq
					elif msg.mid == "0CAN" or msg.mid == "0Tx":
						pass
					else:
						print "Increased message frequency detected"
						print msg.mid + msg.TS
						print float(message[msg.mid]["Message Frequency"])
						print "Normal Message Timing"
						print float(msg_timing[msg.mid]["Message Frequency"])
						past_anomalies = message[msg.mid]["Anomaly Counter"]
						message[msg.mid]["Anomaly Counter"] = message[msg.mid]["Anomaly Counter"] + 1
					if message[msg.mid]["Anomaly Counter"] > 5:
						detectors.anomaly_log.update({msg.mid: message[msg.mid]})
						detectors.message_detector_status = 1
						anomalies += (message[msg.mid]["Anomaly Counter"]-past_anomalies)
						if (float(anomalies)/i) > 0.2:
							detectors.dos_status = 1
							print message[msg.mid]
					else:
						pass
			except KeyError:
				m = {msg.mid: {"Time Stamp": msg.TS, "Message Frequency": msg_freq, "Message Interval": msg_interval}}
				new_message.update(m)
				if (float(new_message[msg.mid]["Message Frequency"]) - (float(new_message[msg.mid]["Message Frequency"]) * 0.5)) \
					<= float(message[msg.mid]["Message Frequency"]) <= \
					(float(new_message[msg.mid]["Message Frequency"]) + (float(new_message[msg.mid]["Message Frequency"]) * 0.5)):
					# Check that frequency is within range
					#L = (float(msg_timing[msg.mid]["Message Frequency"]) - (float(msg_timing[msg.mid]["Message Frequency"]) * 0.25))
					#H = (float(msg_timing[msg.mid]["Message Frequency"]) + (float(msg_timing[msg.mid]["Message Frequency"]) * 0.25))
					#print "Message: %s, Current: %s, Range: %s, %s" % (msg.mid, message[msg.mid]["Message Frequency"], L,H)
					pass
				else:
					if msg.mid == "0CAN" or msg.mid == "0Tx":
						pass
					else:
						print "Increased message frequency detected"
						print msg.mid + msg.TS
						print float(message[msg.mid]["Message Frequency"])
						print "New Normal Message Timing"
						print float(new_message[msg.mid]["Message Frequency"])
						message[msg.mid]["Anomaly Counter"] = message[msg.mid]["Anomaly Counter"] + 1
					if message[msg.mid]["Anomaly Counter"] > 5:
						detectors.anomaly_log.append([msg.mid, message[msg.mid]])
						detectors.message_detector_status = 1
					else:
						pass
	print "anomalies", detectors.anomaly_log
	f.close()


# Checks Frequency of the Messages and indicates if timing is out of normal range.
def frequency_checker(input_file, mids, message_frequencies):

	# {"Msg_ID": msg.mid, "Time Stamp": msg.ts, "Frequency Range": x}
	time_interval = 2
	msg_id = []
	old_frequency = {}
	message = {}
	msg_frequencies = message_frequencies
	time = 0
	# Message Frequency Range constant for each MID
	# {msg.mid: message_freq}
	f = open(input_file, "r")
	for line in f:
		# Get Message ID
		# Raw Data
		msg = Converter(line)
		# M&V Data
		#msg = Message(line)
		# Need to set up frequency and time for every MSG ID before frequency check
		if time == 0:
			time = msg.TS
		else:
			pass
		if msg.mid not in msg_id:
			msg_id.append(msg.mid)
			u = {msg.mid: {"Time Stamp": msg.TS, "Message Frequency": 0, "Message Count": 1}}
			message.update(u)
			t = {msg.mid: {"Time Stamp": msg.TS, "Message Frequency": 0, "Message Count": 0}}
			old_frequency.update(t)
		elif msg.mid in msg_id:
			# Calculate Message Frequency
			if float(msg.TS) - float(time) < time_interval:
				message[msg.mid]["Message Count"] += 1
				message[msg.mid]["Time Stamp"] = msg.TS
			else:
				for i in msg_id:
					frequency = message[i]["Message Count"]
					if old_frequency[i]["Message Frequency"] == 0:
						old_frequency[i]["Message Frequency"] = frequency
						message[i]["Message Frequency"] = frequency
						message[i]["Message Count"] = 0
					else:
						message[i]["Message Frequency"] = frequency
						old_frequency[i]["Message Frequency"] = frequency
						try:
							if (msg_frequencies[i]["Message Frequency"] - (msg_frequencies[i]["Message Frequency"] * 0.9)) \
								<= message[i]["Message Frequency"] <= \
								(msg_frequencies[i]["Message Frequency"] + (msg_frequencies[i]["Message Frequency"] * 0.9)):
								# Check that frequency is within range
								#L = (float(msg_timing[msg.mid]["Message Frequency"]) - (float(msg_timing[msg.mid]["Message Frequency"]) * 0.25))
								#H = (float(msg_timing[msg.mid]["Message Frequency"]) + (float(msg_timing[msg.mid]["Message Frequency"]) * 0.25))
								#print "Message: %s, Current: %s, Range: %s, %s" % (msg.mid, message[msg.mid]["Message Frequency"], L,H)
								pass
							else:
								if i == "0CAN" or i == "0Tx" or i == "0Rx" or i == "Rx_Tx_Error":
									# CANspy errors not errors with vehicle CAN
									pass
								else:
									a = {i: {"Time Stamp": msg.TS, "Normal Frequency": msg_frequencies[i]["Message Frequency"],
								 	"Frequency": message[i]["Message Frequency"]}}
									detectors.anomaly_log_freq.append(a)
									detectors.message_detector_status = 1
						except KeyError:
							# Add new CAN IDs
							#print "new key", i
							u = {i: {"Time Stamp": msg.TS, "Message Frequency": 0, "Message Count": 1}}
							msg_frequencies.update(u)
							t = {i: {"Time Stamp": msg.TS, "Message Frequency": 0, "Message Count": 0}}
							old_frequency.update(t)
						message[i]["Message Count"] = 0
				time = msg.TS

	print "Anomalies", detectors.anomaly_log_freq

	f.close()