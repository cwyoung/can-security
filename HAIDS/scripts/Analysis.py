#! /usr/bin/python
from Message import Message, Converter
import sys, os
import argparse
import csv
import matplotlib.pyplot as plt
import re
import numpy as np
from numpy import fft

def main():
	# Get Analysis options
	parser = argparse.ArgumentParser()
	# Get Input File
	parser.add_argument('--input', '-i', action="store", dest="input", help="Input file for analysis")
	# Get Output File
	parser.add_argument('--output', '-o', action="store", dest="output", help="Output file")
	# Get Attack File
	parser.add_argument('--attack', '-k', action="store", dest="attack", help="File of attack data")
	# Get Analysis type
	parser.add_argument('--analysis', '-a', action="store", dest="analysis", help="Select type of analysis")
	# Get CAN IDs
	parser.add_argument('--id', '-m', action="append", dest='ids', default=[])
	parser_results = parser.parse_args()

	# Call desired analysis
	if parser_results.analysis == "interval":
		interval_analysis(parser_results.input, parser_results.attack)
	elif parser_results.analysis == "frequency":
		frequency_analysis(parser_results.input, parser_results.attack)
	elif parser_results.analysis == "metrics":
		get_metrics(parser_results.input)
	elif parser_results.analysis == "analysis":
		CAN_message(parser_results.input, parser_results.output)
	elif parser_results == "fdomain":
		frequency_domain(parser_results.input)


def CAN_message(input_file, output_file):
	raw_message = "Time, Relative time, status, error, tx, description, network, node, mid, target, source, data bits"
	f = open(input_file, "r")
	# Output all converted CAN messages into a log
	out = open(output_file, "wb")
	out.write(raw_message + '\n')
	# Log of all CAN messages
	CAN_log = []
	ID_log = {}
	mids = []
	for line in f:
		try:
			# This parses raw data and identifies raw data fields into CAN fields, essentially csv()
			msg = Converter(line)
			# Only get CAN data and ignore header data
			try:
				float(msg.TS)
				# Identifies CAN message fields from raw data
				message = msg.time, msg.status, msg.error, msg.tx, msg.description, msg.network, msg.node, msg.mid, \
						  msg.target, msg.source, msg.data
				message_str = str(message)
				CAN_log.append(message)
				out.write(message_str + '\n')
				if msg.mid not in mids:
					mids.append(msg.mid)
					m = {msg.mid: {"Time Stamp": [msg.TS], "Data": [msg.data]}}
					ID_log.update(m)
				elif msg.mid in mids:
					ID_log[msg.mid]["Time Stamp"].append(msg.TS)
					ID_log[msg.mid]["Data"].append(msg.data)
			except ValueError:
				pass
		except IndexError:
			pass
	out.close()
	for i in ID_log:
		x_axis = []
		y_axis = []
		for x, y in enumerate(ID_log[i]["Time Stamp"]):
			try:
				interval = float(ID_log[i]["Time Stamp"][x + 1]) - float(ID_log[i]["Time Stamp"][x])
				x_axis.append(float(ID_log[i]["Time Stamp"][x]))
				y_axis.append(interval)
			except IndexError:
				pass
		plt.scatter(x_axis, y_axis, color="green", marker='o')
		plt.xlabel("Time (sec)")
		plt.ylabel("Interval (sec)")
		plt.title("CAN ID: %s" % i)
		plot_name = "CAN_ID %s" % i
		try:
			plt.xlim(min(x_axis), max(x_axis))
			plt.xticks([min(x_axis), max(x_axis)])
			plt.ylim([0, max(y_axis)*1.1])
			plt.yticks([0, max(y_axis)])
		except ValueError:
			plt.xlim(0,100)
		plt.ioff()
		plt.savefig('Plots/'+plot_name)
		plt.clf()
	print "Plots saved"
	return CAN_log


def CAN_metrics_plot(input_file):
	# Plot Individual CAN message timing metrics
	message_metrics = get_metrics(input_file)
	for i in message_metrics:
		x = message_metrics[i]["Time Stamp"]
		y = message_metrics[i]["Message Interval"]
		f_x = message_metrics[i]["Message Frequency"]["Time"]
		f_y = message_metrics[i]["Message Frequency"]["Frequency"]
		fig, ax = plt.subplots(nrows=2, ncols=1, sharex='col')
		ax[0].scatter(x, y, label='stars', color='blue')
		ax[0].set(xlabel='Time', ylabel='Interval')
		ax[0].set_title("CAN ID: %s" % i)
		ax[1].plot(f_x, f_y, marker='*', color='green')
		ax[1].set(xlabel='Time', ylabel='Frequency')
		try:
			plt.xlim([min(x), max(x)])
			plt.xticks([min(x), max(x)])
		except ValueError:
			plt.xlim(0, 100)
		plot_name = "CAN ID %s" % i
		path = "Test_Plots" + plot_name
		save_plot(path)


# Gets message timing features from CAN log
def get_metrics(input_file):
	message_timings = {}
	time_old = {}
	mids = []
	time = 0
	time_interval = 1
	message_log = {}
	f = open(input_file, "r")
	for line in f:
		try:
			msg = Converter(line)
			prog = re.match(r'\d+,[-+]?[0-9]*\.?[0-9]*,.+',line)
			if prog:
				try:
					if time == 0:
						time = msg.TS
					else:
						pass
					if msg.mid not in mids:
							mids.append(msg.mid)
							u = {msg.mid: {"Time Stamp": msg.TS, "Message Frequency": 0, "Message Interval": 0, "Message Count": 1, "Intervals": []}}
							message_timings.update(u)
							t = {msg.mid: {"Time Stamp": msg.TS, "Message Interval": 0, "Message Frequency": 0, "Message Count": 0}}
							time_old.update(t)
							m = {msg.mid: {"Time Stamp": [], "Message Interval": [], "Message Frequency": {"Time": [], "Frequency": []}}}
							message_log.update(m)
					elif msg.mid in mids:
							# Calculate Message interval
							msg_interval = float(msg.TS) - float(time_old[msg.mid]["Time Stamp"])
							time_old[msg.mid]["Time Stamp"] = msg.TS
							time_old[msg.mid]["Message Interval"] = msg_interval
							message_timings[msg.mid]["Message Interval"] = msg_interval
							#message_log[msg.mid]["Time Stamp"].append(float(msg.TS))
							#message_log[msg.mid]["Message Interval"].append(msg_interval)
							# Calculate message frequency
							if float(msg.TS) - float(time) < time_interval:
								# Count messages and sum message intervals
								message_timings[msg.mid]["Message Count"] += 1
								message_timings[msg.mid]["Time Stamp"] = msg.TS
								message_timings[msg.mid]["Intervals"].append(msg_interval)
							else:
								for i in mids:
									# Calculate Average interval for all CAN IDs
									n = len(message_timings[i]["Intervals"])
									try:
										interval = sum(float(a) for a in message_timings[i]["Intervals"]) / n
									except ZeroDivisionError:
										pass
									message_log[i]["Time Stamp"].append(float(msg.TS))
									message_log[i]["Message Interval"].append(interval)
									message_timings[i]["Intervals"] = []
									# Calculate frequency
									frequency = message_timings[i]["Message Count"]
									if time_old[i]["Message Frequency"] == 0:
										time_old[i]["Message Frequency"] = frequency
										message_timings[i]["Message Frequency"] = frequency
										message_timings[i]["Message Count"] = 0
										message_log[i]["Message Frequency"]["Time"].append(float(msg.TS))
										message_log[i]["Message Frequency"]["Frequency"].append(float(frequency))
									else:
										average_frequency = (frequency+(time_old[i]["Message Frequency"]))/2
										time_old[i]["Message Frequency"] = frequency
										message_timings[i]["Message Frequency"] = average_frequency
										message_timings[i]["Message Count"] = 0
										message_log[i]["Message Frequency"]["Time"].append(float(msg.TS))
										message_log[i]["Message Frequency"]["Frequency"].append(frequency)
									time = msg.TS
				except ValueError:
					pass
		except IndexError:
			pass
	f.close()
	print "Got Metrics"
	return message_log


def interval_analysis(input_file, attack_file):
	# Compare Normal and Attack message intervals with plots
	normal = get_metrics(input_file)
	malicious = get_metrics(attack_file)

	for i in normal:
		# Time stamp
		t = normal[i]["Time Stamp"]
		# Intervals
		interval = normal[i]["Message Interval"]
		# Plot Intervals for CAN ID
		plt.scatter(t, interval, color="blue", marker="*")
		try:
			t_attack = malicious[i]["Time Stamp"]
			i_attack = malicious[i]["Message Interval"]
		except KeyError:
			t_attack = []
			i_attack = []
		# Plot Attack Intervals
		plt.scatter(t_attack, i_attack, color="red", marker='o')
		plt.xlabel("Time (sec)")
		plt.ylabel("Interval (sec)")
		plt.title("CAN ID: %s" % i)
		try:
			plt.xlim([min(t_attack), max(t_attack)])
			plt.xticks([min(t_attack), max(t_attack)])
			plt.ylim([0, max(interval) * 1.1])
			plt.yticks([0, max(interval)])
		except ValueError:
			plt.xlim(0, 100)
		plot_name = "CAN ID %s" % i
		path = "Interval_Plots/" + plot_name
		save_plot(path)


def frequency_analysis(input_file, attack_file):
	# Compare Normal and Attack message frequencies with plots
	normal = get_metrics(input_file)
	malicious = get_metrics(attack_file)
	for i in normal:
		del normal[i]["Message Frequency"]["Time"][0]
		del normal[i]["Message Frequency"]["Frequency"][0]
		x = normal[i]["Message Frequency"]["Time"]
		y = normal[i]["Message Frequency"]["Frequency"]
		plt.scatter(x, y, color='blue', marker='*')
		try:
			x_attack = malicious[i]["Message Frequency"]["Time"]
			y_attack = malicious[i]["Message Frequency"]["Frequency"]
		except KeyError:
			x_attack = []
			y_attack = []
		plt.scatter(x_attack, y_attack, color='red', marker='o')
		plt.xlabel("Time (sec)")
		plt.ylabel("Frequency")
		try:
			plt.xlim([min(x), max(x)])
			plt.xticks([min(x), max(x)])
			plt.ylim([0, max(y) * 1.1])
			plt.yticks([0, max(y)])
		except ValueError:
			plt.xlim(0, 200)
		plt.title("CAN ID: %s" % i)
		plot_name = "CAN ID %s" % i
		path = "Frequency_Plots/" + plot_name
		save_plot(path)


def freq_domain_test():
	Fs = 150.0  # sampling rate
	Ts = 1.0/Fs  # sampling interval
	t = np.arange(0,1,Ts)  # time vector
	ff = 5  # sampling rate
	y = np.sin(2*np.pi*ff*t)

	n = len(y)  # length of signal
	k = np.arange(n)
	T = n/Fs
	frq = k/T # 2 side frequency range
	frq = frq[range(n/2)] # 1 side frequency range
	Y = fft.fft(y)/2  # fft computing and normalization
	Y = Y[range(n/2)]

	fig, ax = plt.subplots(2,1)
	ax[0].plot(t, y)
	ax[0].set_xlabel('Time')
	ax[0].set_ylabel('Amplitude')
	ax[1].plot(frq, abs(Y), 'r')  # plotting the spectrum
	ax[1].set_xlabel('Freq (Hz)')
	ax[1].set_ylabel('|Y(freq)|')
	plt.show()


def frequency_domain(input_file):
	data = get_metrics(input_file)
	for i in data:
		time = data[i]["Message Frequency"]["Time"]
		frequency = data[i]["Message Frequency"]["Frequency"]
		# Number of data points
		n = len(frequency)
		k = np.arange(n)
		mag = fft.fft()
		Freq = fft.fftfreq()
		fig, ax = plt.subplots(2, 1)
		ax[0].set_title("CAN ID: %s" % i)
		ax[0].plot(time, frequency)
		ax[0].set_xlabel('Time')
		ax[0].set_ylabel('Frequency')
		#ax[1].plot(xf[1:], 2.0 / n * np.abs(mag[0:n // 2])[1:])  # plotting the spectrum
		ax[1].plot(mag, Freq)
		# ax[1].plot(Freq, np.abs(Y))
		ax[1].set_xlabel('Freq (Hz)')
		ax[1].set_ylabel('|Y(freq)|')
		plt.show()


# Plot saving function that will save each figure individually in user specified directory
def save_plot(path, ext='png', close=True, verbose=True):

	# Extract directory and filename from given path
	directory = os.path.split(path)[0]
	filename = "%s.%s" % (os.path.split(path)[1], ext)
	if directory == '':
		directory = '.'
	# Create directory if needed
	if not os.path.exists(directory):
		os.makedirs(directory)

	# Final path to save to
	savepath = os.path.join(directory, filename)
	if verbose:
		print "Saving figure to '%s'" % savepath
	# Save figure
	plt.savefig(savepath)
	# Clear figure
	plt.clf()


if __name__ == '__main__':

	main()
	#frequency_domain("raw_data.csv")
	#freq_domain_test()
	# python Analysis.py -a [interval, frequency, analysis] -i [raw_data.csv] -k [attack_data.csv]